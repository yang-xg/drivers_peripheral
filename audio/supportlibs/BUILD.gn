# Copyright (c) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//drivers/hdf_core/adapter/uhdf2/uhdf.gni")
import("//drivers/peripheral/audio/audio.gni")

config("audio_interface_config") {
  visibility = [ ":*" ]

  cflags = [
    "-Wall",
    "-Wextra",
    "-Werror",
    "-DGST_DISABLE_DEPRECATED",
    "-DHAVE_CONFIG_H",
    "-fno-strict-aliasing",
    "-Wno-sign-compare",
    "-Wno-builtin-requires-header",
    "-Wno-implicit-function-declaration",
    "-Wno-format",
    "-Wno-int-conversion",
    "-Wno-unused-function",
    "-Wno-unused-parameter",
    "-Wno-thread-safety-attributes",
    "-Wno-missing-field-initializers",
    "-Wno-inconsistent-missing-override",
    "-fno-rtti",
    "-fno-exceptions",
    "-ffunction-sections",
    "-fdata-sections",
  ]
  if (drivers_peripheral_audio_feature_mono_to_stereo) {
    cflags += [ "-DMONO_TO_STEREO" ]
  }
  if (enable_audio_alsa_mode) {
    cflags += [ "-DALSA_MODE" ]
  }

  ldflags = [ "-Wl" ]
}

if (defined(ohos_lite)) {
  ohos_shared_library("hdi_audio_interface_lib_capture") {
    sources = [
      "//drivers/peripheral/audio/hal/hdi_passthrough/src/audio_common.c",
      "adm_adapter/src/audio_interface_lib_capture.c",
      "adm_adapter/src/audio_interface_lib_common.c",
    ]

    include_dirs = [
      "//drivers/peripheral/audio/hal/hdi_passthrough/include",
      "//drivers/peripheral/audio/supportlibs/adm_adapter/include",
      "//drivers/peripheral/audio/supportlibs/interfaces/include",
      "//drivers/peripheral/audio/interfaces/include",
      "//third_party/bounds_checking_function/include",
      "//base/hiviewdfx/hilog_lite/interfaces/native/kits/",
      "//device/hisilicon/hispark_taurus/sdk_linux/huawei_proprietary/include",
    ]

    deps = [
      "//base/hiviewdfx/hilog_lite/frameworks/featured:hilog_shared",
      "//third_party/bounds_checking_function:libsec_shared",
    ]
    external_deps = [ "hdf_core:libhdf_utils" ]
    public_configs = [ ":audio_interface_config" ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }

  ohos_shared_library("hdi_audio_interface_lib_render") {
    sources = [
      "//drivers/peripheral/audio/hal/hdi_passthrough/src/audio_common.c",
      "adm_adapter/src/audio_interface_lib_common.c",
      "adm_adapter/src/audio_interface_lib_render.c",
    ]

    include_dirs = [
      "//drivers/peripheral/audio/hal/hdi_passthrough/include",
      "//drivers/peripheral/audio/supportlibs/adm_adapter/include",
      "//drivers/peripheral/audio/supportlibs/interfaces/include",
      "//drivers/peripheral/audio/interfaces/include",
      "//third_party/bounds_checking_function/include",
      "//base/hiviewdfx/hilog_lite/interfaces/native/kits/",
      "//device/hisilicon/hispark_taurus/sdk_linux/huawei_proprietary/include",
    ]

    deps = [
      "//base/hiviewdfx/hilog_lite/frameworks/featured:hilog_shared",
      "//third_party/bounds_checking_function:libsec_shared",
    ]
    external_deps = [ "hdf_core:libhdf_utils" ]
    if (enable_audio_hal_hdf_log) {
      defines = [ "AUDIO_HAL_HDF_LOG" ]
    }
    public_configs = [ ":audio_interface_config" ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }
} else {
  ohos_shared_library("hdi_audio_interface_lib_capture") {
    sources =
        [ "//drivers/peripheral/audio/hal/hdi_passthrough/src/audio_common.c" ]
    if (defined(enable_audio_alsa_lib) && enable_audio_alsa_lib == true) {
      sources += [
        "//drivers/peripheral/audio/supportlibs/alsa_adapter/src/alsa_lib_capture.c",
        "//drivers/peripheral/audio/supportlibs/alsa_adapter/src/alsa_lib_common.c",
      ]
    } else {
      sources += [
        "//drivers/peripheral/audio/supportlibs/adm_adapter/src/audio_interface_lib_capture.c",
        "//drivers/peripheral/audio/supportlibs/adm_adapter/src/audio_interface_lib_common.c",
      ]
    }
    include_dirs = [
      "//drivers/peripheral/audio/hal/hdi_passthrough/include",
      "//drivers/peripheral/audio/interfaces/include",
      "//drivers/peripheral/audio/supportlibs/interfaces/include",
      "//third_party/bounds_checking_function/include",
      "//device/hisilicon/hispark_taurus/sdk_linux/huawei_proprietary/include",
    ]

    deps = []

    if (enable_audio_hal_hdf_log) {
      defines = [ "AUDIO_HAL_HDF_LOG" ]
    }

    if (enable_audio_alsa_mode) {
      sources += [
        "//drivers/peripheral/audio/supportlibs/tinyalsa_adapter/src/alsa_mixer.c",
        "//drivers/peripheral/audio/supportlibs/tinyalsa_adapter/src/audio_hw.c",
        "//drivers/peripheral/audio/supportlibs/tinyalsa_adapter/src/route_config.c",
      ]
      include_dirs += [
        "//third_party/tinyalsa/include",
        "//drivers/peripheral/audio/supportlibs/tinyalsa_adapter/include",
      ]
      deps += [ "//third_party/tinyalsa:libtinyalsa" ]
    }
    if (defined(enable_audio_alsa_lib) && enable_audio_alsa_lib == true) {
      include_dirs += [
        "//third_party/alsa-lib/include",
        "//drivers/peripheral/audio/supportlibs/alsa_adapter/include",
      ]
      deps += [ "//third_party/alsa-lib:libasound" ]
    } else {
      include_dirs +=
          [ "//drivers/peripheral/audio/supportlibs/adm_adapter/include" ]
    }
    if (is_standard_system) {
      external_deps = [
        "hdf_core:libhdf_host",
        "hdf_core:libhdf_ipc_adapter",
        "hdf_core:libhdf_utils",
        "hdf_core:libhdi",
        "hiviewdfx_hilog_native:libhilog",
        "utils_base:utils",
      ]
    } else {
      external_deps = [ "hilog:libhilog" ]
    }

    public_configs = [ ":audio_interface_config" ]

    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }

  ohos_shared_library("hdi_audio_interface_lib_render") {
    sources =
        [ "//drivers/peripheral/audio/hal/hdi_passthrough/src/audio_common.c" ]
    if (defined(enable_audio_alsa_lib) && enable_audio_alsa_lib == true) {
      sources += [
        "//drivers/peripheral/audio/supportlibs/alsa_adapter/src/alsa_lib_common.c",
        "//drivers/peripheral/audio/supportlibs/alsa_adapter/src/alsa_lib_render.c",
      ]
    } else {
      sources += [
        "//drivers/peripheral/audio/supportlibs/adm_adapter/src/audio_interface_lib_common.c",
        "//drivers/peripheral/audio/supportlibs/adm_adapter/src/audio_interface_lib_render.c",
      ]
    }

    include_dirs = [
      "//drivers/peripheral/audio/hal/hdi_passthrough/include",
      "//drivers/peripheral/audio/interfaces/include",
      "//drivers/peripheral/audio/supportlibs/interfaces/include",
      "//third_party/bounds_checking_function/include",
      "//device/hisilicon/hispark_taurus/sdk_linux/huawei_proprietary/include",
    ]

    deps = []

    if (enable_audio_hal_hdf_log) {
      defines = [ "AUDIO_HAL_HDF_LOG" ]
    }

    if (enable_audio_alsa_mode) {
      sources += [
        "//drivers/peripheral/audio/supportlibs/tinyalsa_adapter/src/alsa_mixer.c",
        "//drivers/peripheral/audio/supportlibs/tinyalsa_adapter/src/audio_hw.c",
        "//drivers/peripheral/audio/supportlibs/tinyalsa_adapter/src/route_config.c",
      ]
      include_dirs += [
        "//third_party/tinyalsa/include",
        "//drivers/peripheral/audio/supportlibs/tinyalsa_adapter/include",
      ]
      deps += [ "//third_party/tinyalsa:libtinyalsa" ]
    }
    if (defined(enable_audio_alsa_lib) && enable_audio_alsa_lib == true) {
      include_dirs += [
        "//drivers/peripheral/audio/supportlibs/alsa_adapter/include",
        "//third_party/alsa-lib/include",
      ]
      deps += [ "//third_party/alsa-lib:libasound" ]
    } else {
      include_dirs +=
          [ "//drivers/peripheral/audio/supportlibs/adm_adapter/include" ]
    }
    if (is_standard_system) {
      external_deps = [
        "hdf_core:libhdf_host",
        "hdf_core:libhdf_ipc_adapter",
        "hdf_core:libhdf_utils",
        "hdf_core:libhdi",
        "hiviewdfx_hilog_native:libhilog",
        "utils_base:utils",
      ]
    } else {
      external_deps = [ "hilog:libhilog" ]
    }

    public_configs = [ ":audio_interface_config" ]

    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }
}
